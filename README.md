# gitlab-template-python

> Python Project Template

[![GitLab: Tag](https://img.shields.io/gitlab/v/tag/deadnews/gitlab-template-python?logo=gitlab&logoColor=white)](https://gitlab.com/deadnews/gitlab-template-python)
[![GitLab: Release](https://img.shields.io/gitlab/v/release/deadnews/gitlab-template-python?logo=gitlab&logoColor=white)](https://gitlab.com/deadnews/gitlab-template-python/-/releases)
[![Docker: gitlab](https://img.shields.io/badge/docker-gray.svg?logo=docker&logoColor=white)](https://gitlab.com/deadnews/gitlab-template-python/container_registry/6380376)
[![Documentation](https://img.shields.io/badge/documentation-gray.svg?logo=materialformkdocs&logoColor=white)](https://deadnews.gitlab.io/gitlab-template-python)
[![GitLab: Coverage](https://gitlab.com/deadnews/gitlab-template-python/badges/main/coverage.svg)](https://gitlab.com/deadnews/gitlab-template-python/-/graphs/main/charts)

## Installation

Package

```sh
pipx install gitlab-template-python --index-url https://gitlab.com/api/v4/projects/57816178/packages/pypi/simple
```

Docker

```sh
docker pull registry.gitlab.com/deadnews/gitlab-template-python:latest
```
