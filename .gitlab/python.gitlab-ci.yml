image: python:3.13

variables:
  POETRY_VIRTUALENVS_IN_PROJECT: "true"
  PIP_CACHE_DIR: ${CI_PROJECT_DIR}/.cache/pip

cache:
  - key: pip-${CI_JOB_IMAGE}
    paths:
      - ${PIP_CACHE_DIR}
  - key:
      prefix: poetry-${CI_JOB_IMAGE}
      files:
        - poetry.lock
    paths:
      - ${CI_PROJECT_DIR}/.venv

lint:
  stage: test
  rules:
    - exists: [pyproject.toml]
  before_script:
    - pip install poetry
    - poetry install
  script:
    - poetry run poe lint

tests:
  stage: test
  image: $IMAGE
  rules:
    - exists: [pyproject.toml]
  parallel:
    matrix:
      - PLATFORM: linux
        IMAGE: ["python:3.12", "python:3.13"]
  before_script:
    - pip install poetry
    - poetry install
  script:
    - poetry run poe test
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

deploy:
  stage: deploy
  needs:
    - lint
    - tests
  rules:
    - if: $CI_COMMIT_TAG
    - when: manual
      allow_failure: true
  before_script:
    - pip install poetry poetry-dynamic-versioning[plugin]
  script:
    - poetry build
    - poetry config repositories.gitlab "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi"
    - poetry config http-basic.gitlab gitlab-ci-token "$CI_JOB_TOKEN"
    - poetry publish --repository gitlab

pages:
  stage: deploy
  needs:
    - lint
    - tests
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      exists: [mkdocs.yml]
      changes:
        - docs
        - src
        - mkdocs.yml
        - CHANGELOG.md
        - README.md
  artifacts:
    paths:
      - public
  before_script:
    - pip install poetry
    - poetry install
  script:
    - poetry run mkdocs build --site-dir public
